/* LinSpace
   Wouter M. Koolen <wmkoolen@gmail.com>

   Simple linear range
*/

#include <cassert>

struct LinSpace {
  double min;
  double max;
  int steps;

  LinSpace(double min, double max, int steps) :
    min(min), max(max), steps(steps)
  {}
    

  double get(int i) const {
    assert(i >= 0);
    assert(i < steps);
    return min + (max-min)*i/(steps-1);
  }
};
