/* Squint implementation unit tests.
   Wouter M. Koolen <wmkoolen@gmail.com>

   Checks that the implementation agrees with the simple expression
   (whenever the latter is numerically stable)
 */

#include <iostream>
#include <cfenv>
#include "squint.h"
#include "LinSpace.h"
using namespace std;


// check that lnerfc is monotonic
template<class real>
void test_lnerfc_monotonic() {
  LinSpace Xs(0, 10000, 10000000);
  real prev = INFINITY;
  for (int xi = 0; xi < Xs.steps; ++xi) {
    real x = Xs.get(xi);
    real cur = Squint<real>::lnerfc(x);
    assert(cur < prev);
    prev = cur;
  }
  cerr << "lnerfc monotonic" << endl;
}

// check that lnerfc accurately reproduces the direct computation
template<class real>
void test_lnerfc_accurate() {
  feclearexcept(FE_UNDERFLOW);
  
  real worstp = -INFINITY;
  real worstc;

  LinSpace Xs(0, 10000, 10000000);
  for (int xi = 0; xi < Xs.steps; ++xi) {
    real x = Xs.get(xi);
    assert(!fetestexcept(FE_UNDERFLOW));
    real v = Squint<real>::lnerfc(x); // careful computation
    assert(!fetestexcept(FE_UNDERFLOW));
    real a = log(erfc(x)); // rough computation
    if (fetestexcept(FE_UNDERFLOW) ||
	std::isinf(a) /* hack around Clang missing fp exception support */) {
      feclearexcept(FE_UNDERFLOW);
      break; // no use going higher, will keep undeflowing
    }
    real p = log10(abs(v-a));
    feclearexcept(FE_UNDERFLOW); // Clang fp exceptions are broken
    if (p > worstp) {
      worstp = p;
      worstc = x;
    }
  }
  cerr << "lnerfc accurate to " << worstp << " at x=" << worstc << endl;
}

// check that lnerfd accurately reproduces the direct computation
template<class real>
void test_lnerfd_accurate() {
  // check lnerfd reasonable
  real worstp = -INFINITY;
  pair<real, real> worstc;
  LinSpace Xs(-200, 200, 1000);
  for (int xi = 0; xi < Xs.steps; ++xi) {
    real l = Xs.get(xi);
    for (int xj = xi+1; xj < Xs.steps; ++xj) {
      real r = Xs.get(xj);

      assert(!fetestexcept(FE_UNDERFLOW));
      real v = Squint<real>::lnerfd(l, r);
      feclearexcept(FE_UNDERFLOW);

      // take care to reflect throug origin if both are positive
      real erfcr = erfc(r <= 0 ? -r : l);
      real erfcl = erfc(r <= 0 ? -l : r);
      if (fetestexcept(FE_UNDERFLOW)) {
	feclearexcept(FE_UNDERFLOW);
	continue;
      }
      assert(erfcr > erfcl);
      assert(erfcl>0);

      if (erfcr == 2) continue;

      real a = log(erfcr-erfcl);
      real p = log10(abs(v-a));
      if (p > worstp) {
	worstp = p;
	worstc = make_pair(l,r);
      }
    }
  }
  cerr << "lnerfd accurate to " << worstp << " at l=" << worstc.first << " r=" << worstc.second << endl;
}



// check that lnevidence accurately reproduces the direct computation
template<class real>
void test_lnevidence_accurate(int T) {
  real worstp = -INFINITY;
  pair<real, real> worstc;

  LinSpace Rs(-T, T, 1001);
  LinSpace Vs( 0, T, 1000);
  
  for (int vi = 0; vi < Vs.steps; ++vi) {
    real V = Vs.get(vi);
    for (int ri = 0; ri < Rs.steps; ++ri) {
      real R = Rs.get(ri);

      real v = Squint<real>::lnevidence(R, V);

      if (::isinf(v)) {
	cerr << "R=" << R << " V=" << V << endl;
      }

      assert(!::isinf(v));
      
      real q = 2*sqrt(V);
      real a =  V == 0
	? R==0
	  ? -log(2)
	  : log(expm1(R/2)/R)
	: R*R/(4*V) + log(sqrt(Squint<real>::pi)/q) + Squint<real>::lnerfd((R-V)/q, R/q);
      if (::isinf(a)) continue;
      real p = log10(abs(v-a));
      if (p > worstp) {
	worstp = p;
	worstc = make_pair(R, V);
      }
    }
  }
  cerr << "lnevidence(T=" << T <<") accurate to " << worstp << " at R=" << worstc.first << " V=" << worstc.second << endl;
}


// run the above tests
template<class real>
void tests() {
  test_lnerfc_monotonic<real>();
  test_lnerfc_accurate<real>();
  test_lnerfd_accurate<real>();
  test_lnevidence_accurate<real>(1);
  test_lnevidence_accurate<real>(5e5);    
}


int main(int argc, const char ** argv) {
  cout << "double precision:" << endl;
  tests<double>();
  cout << endl;

  cout << "long double precision:" << endl;
  tests<long double>();
  cout << endl;
  
  return 0;
}
