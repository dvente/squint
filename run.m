%% run the Squint algorithm on simple stochastic IID losses
%% Wouter M. Koolen <wmkoolen@gmail.com>

%% check that squint is compiled
if exist('lnevidence', 'file') ~= 3
  error('squint:notfound', 'I could not locate the main Squint function ''lnevidence''.\nPlease compile it for your environment using either ''make matlab'' or ''make octave''.\n');
end


%% Setup
K = 5;   % number of experts
T = 1e5; % number of rounds

% prior distribution on experts (drawn uniformly at random from simplex)
pi = diff([0 sort(rand(1,K-1)) 1]);

% loss rates of the experts
rates = rand(1, K);  % random rates (typically easy data)
rates = ones(1,K)/2; % equal uniform rates (worst-case type data)

% table of instantaneous expert losses. IID Bernoulli.
losses = rand(T, K) <= repmat(rates, T, 1);

%% Run Squint
[lalg, R, V] = run_squint(pi, losses);


%% Graph
% compute cumulative losses
Losses = cumsum([zeros(1,K); losses]);
Lalg   = cumsum([0 ; lalg]);
Lstar  = min(Losses,[], 2);  % cumulative loss of best expert

% set up array of plot titles
lg = arrayfun(@(n) sprintf('k = %d',n), 1:K, 'UniformOutput', false);

% plot cumulative regret plots
subplot(1,2,1);
plot(0:T, [Lalg Losses] - repmat(Lstar, 1, K+1));
title('cumulative regret');
legend(['alg', lg], 'location', 'NorthWest');
xlabel('T');

% plot regret/variance trajectories
maxV = ceil(max(V(end,:))); % range of variances
subplot(1,2,2); 
plot(0:maxV, 2*sqrt(0:maxV), ...
     V, R);
title('regret/variance trajectories');
legend(['2 sqrt(V)' lg], 'location', 'NorthWest')
xlabel('V');
ylabel('R');
