# Squint README #

This repository contains a numerically stable implementation of the Squint algorithm from the paper

*
[Second-Order Quantile Methods for Experts and Combinatorial Games](http://jmlr.csail.mit.edu/proceedings/papers/v40/Koolen15a.pdf)  
Koolen, Wouter M., and Tim van Erven  
In Proceedings of the 28th Annual Conference on Learning Theory (COLT) 2015, 1155–75
*

It also contains an example Matlab/Octave script, [run.m](run.m), that runs Squint on IID random data.



## Requirements ##
* The implementation is in C++. It comes with a Makefile for compilation using GCC or Clang.
* The example script works in both Matlab and Octave.

## Set up ##
1. Get the source code by cloning this git repository
2. Run 'make octave' or 'make matlab'
3. Fire up octave or matlab
4. Execute 'run'. This will run Squint on stochastic IID data and graph the results.

If you want to see the core code, start with [squint.h](squint.h). If you want to try squint on your data, adapt [run.m](run.m).

## How to run tests ##
The repository contains a battery of unit tests that check the implementation against more direct expressions (which are unstable in some regions of the parameter space).

* Run 'make test' to compile and execute the test executable.
* The reported precision should be around 15 digits for *long double* arithmetic.

## More information ##

* [Squint paper](http://jmlr.csail.mit.edu/proceedings/papers/v40/Koolen15a.pdf)
* [Blog post: Discussion of implementation specifics](http://blog.wouterkoolen.info/Squint_implementation/post.html)
* [Blog post: Performance guarantees supplement](http://blog.wouterkoolen.info/Squint_PAC/post.html)